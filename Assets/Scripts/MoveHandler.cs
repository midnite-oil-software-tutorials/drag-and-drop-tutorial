using MidniteOilSoftware;
using UnityEngine;

public class MoveHandler : MonoBehaviour
{
    [SerializeField] DragAndDropManager _dragAndDropManager;
 //   [SerializeField] GameObject _dropIndicatorPrefab;
 //   [SerializeField] Vector3 _dragOffset = new Vector3(0, 0.1f, 0);

    Transform _startParent;
    Vector3 _startPosition;
//    GameObject _dropIndicator;

    void Start()
    {
        _dragAndDropManager.OnDragStart += HandleDragStart;
        _dragAndDropManager.OnDragged += HandleDragged;
        _dragAndDropManager.OnDragEnd += HandleDragEnd;
    }

    void OnDisable()
    {
        _dragAndDropManager.OnDragStart -= HandleDragStart;
        _dragAndDropManager.OnDragged -= HandleDragged;
        _dragAndDropManager.OnDragEnd -= HandleDragEnd;
    }

    void HandleDragStart(GameObject draggedObject)
    {
        var draggedObjectTransform = draggedObject.transform;
        _startPosition = draggedObjectTransform.localPosition;
        _startParent = draggedObjectTransform.parent;
        draggedObjectTransform.SetParent(null);
 //       draggedObjectTransform.position += _dragOffset;
    }

    void HandleDragged(GameObject draggedObject, Vector3 position, GameObject dropTarget)
    {
        draggedObject.transform.position = position;
        draggedObject.transform.Translate(draggedObject.transform.up * 0.1f);
    }

    void HandleDragEnd(GameObject draggedObject, GameObject dropTarget)
    {
        if (!dropTarget)
        {
            draggedObject.transform.SetParent(_startParent);
            draggedObject.transform.localPosition = _startPosition;
            return;
        }
        draggedObject.transform.SetParent(dropTarget.transform);
        draggedObject.transform.localPosition = _startPosition;
    }
}
