using System;
using MidniteOilSoftware;
using TMPro;
using UnityEngine;

public class DragAndDropUI : MonoBehaviour
{
    [SerializeField] DragAndDropManager _dragAndDropManager;
    [SerializeField] TMP_Text _mousePositionText, _dragActionText, _dropTargetText;
    [SerializeField] GameObject _leftMouseButton, _rightMouseButton;

    void Awake()
    {
        _dragAndDropManager.OnDragStart += HandleDragStart;
        _dragAndDropManager.OnDragged += HandleDragged;
        _dragAndDropManager.OnDragEnd += HandleDragEnd;
    }

    void OnDisable()
    {
        _dragAndDropManager.OnDragStart -= HandleDragStart;
        _dragAndDropManager.OnDragged -= HandleDragged;
        _dragAndDropManager.OnDragEnd -= HandleDragEnd;
    }

    void LateUpdate()
    {
        _mousePositionText.text = $"Mouse Position: {Input.mousePosition}";
        _leftMouseButton.SetActive(Input.GetMouseButton(0));
        _rightMouseButton.SetActive(Input.GetMouseButton(1));
    }
    
    void HandleDragStart(GameObject go)
    {
        _dragActionText.text = $"Drag started on {go.name}";
    }
    
    void HandleDragged(GameObject go, Vector3 position, GameObject dropTarget)
    {
        _dragActionText.text = $"{go.name}Dragged to {position}";
        _dropTargetText.text = $"Drop Target: {dropTarget?.name ?? string.Empty}";
    }
    
    void HandleDragEnd(GameObject go, GameObject dropTarget)
    {
        _dragActionText.text = $"{go.name} drag ended on {dropTarget?.name ?? "nothing"}";
        _dropTargetText.text = $"Drop Target: {dropTarget?.name ?? String.Empty}";
    }
}
