using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class SoundManager : SingletonMonoBehavior<SoundManager>
{
    public void PlayRandomDieRollClip(float volume)
    {
        var clip = GetRandomDieRollClip();
        _audioSource.PlayOneShot(clip, volume);
    }
    
    [SerializeField] AudioClip[] _dieRollClips;
    
    List<AudioClip> _unusedClips;
    AudioSource _audioSource;

    protected override void Awake()
    {
        base.Awake();
        _audioSource = GetComponent<AudioSource>();
        _unusedClips = new List<AudioClip>(_dieRollClips);
        RefreshUnusedClipsList();
    }

    AudioClip GetRandomDieRollClip()
    {
        if (_unusedClips.Count == 0)
        {
            RefreshUnusedClipsList();
        }

        var index = Random.Range(0, _unusedClips.Count);
        var clip = _unusedClips[index];
        _unusedClips.RemoveAt(index);
        return clip;    
    }

    void RefreshUnusedClipsList()
    {
        foreach (var clip in _dieRollClips)
        {
            _unusedClips.Add(clip);
        }
    }
}
