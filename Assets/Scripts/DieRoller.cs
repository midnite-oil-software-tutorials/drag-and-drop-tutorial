using System;
using System.Collections;
using DG.Tweening;
using MidniteOilSoftware;
using UnityEngine;
using Random = UnityEngine.Random;

public class DieRoller : MonoBehaviour
{
    public Action<int> OnDieRolled;
    
    [SerializeField] float _rollForce = 100f;
    [SerializeField] float _torqueAmount = 100f;
    [SerializeField] float _fadeOutDelay = 4f;
    [SerializeField] LayerMask _layerMask;
    [SerializeField] GameObject _clickToRollText;
    
    Rigidbody _rigidbody;
    Transform _transform;
    PulseScale _pulseScale;
    Clickable _clickable;
    bool _rolling;

    void Awake()
    {
        _transform = transform;
        _rigidbody = GetComponent<Rigidbody>();
        _pulseScale = GetComponent<PulseScale>();
        _clickable = GetComponent<Clickable>();
        _clickable.OnMouseDown += OnClick;
        _clickable.SetActive(true);
        _pulseScale.StartPulse();
        _clickToRollText.SetActive(true);
    }

    void Start()
    {
        _rigidbody.constraints = RigidbodyConstraints.FreezeRotation;
    }

    void OnDisable()
    {
        _clickable.OnMouseDown -= OnClick;
    }

    void OnClick(GameObject go)
    {
        RollDie();
    }

    void OnCollisionEnter(Collision collision)
    {
        if (!_rolling) return;
        var volume = Mathf.Clamp(collision.relativeVelocity.magnitude / 5f, 0f, 1f);
        SoundManager.Instance.PlayRandomDieRollClip(volume);
    }

    [ContextMenu("Roll Die")]
    public void RollDie()
    {
        StopAllCoroutines();
        _rigidbody.constraints = RigidbodyConstraints.None;
        _pulseScale.StopPulse();
        _clickToRollText.SetActive(false);
        _clickable.SetActive(false);
        _rolling = true;
        var targetPosition = new Vector3(Random.Range(-1f, 1f), 10f, Random.Range(-1f, 1f));
        var direction = targetPosition - _transform.position;
        _rigidbody.AddForce(direction * _rollForce, ForceMode.Impulse);
        _rigidbody.AddTorque(Random.insideUnitSphere * _torqueAmount, ForceMode.Impulse);
        StartCoroutine(WaitForDieToStop());
        StartCoroutine(FadeOut());
    }

    IEnumerator FadeOut()
    {
        yield return Helpers.GetWaitForSeconds(_fadeOutDelay);
        _transform.DOScale(0, 1f)
            .OnComplete(() => ObjectPoolManager.DespawnGameObject(gameObject));
    }

    IEnumerator WaitForDieToStop()
    {
        var timeOut = Time.time + 5f;
        while (!_rigidbody.IsSleeping() && Time.time < timeOut)
        {
            yield return null;
        }

        yield return null;
        
        var dieValue = GetDieValue();
        Debug.Log($"You rolled a {dieValue}!", gameObject);
        OnDieRolled?.Invoke(dieValue);
    }

    int GetDieValue()
    {
        _rolling = false;
        Vector3[] directions =
        {
            -_transform.forward,     // 1
            -_transform.up,          // 2
            _transform.right,       // 3
            -_transform.right,      // 4
            _transform.up,         // 5
            _transform.forward     // 6
        };

        for (var i = 0; i < directions.Length; i++)
        {
            if (!Physics.Raycast(_transform.position, directions[i], 1f, _layerMask)) continue;
            return i + 1;
        }

        Debug.LogError("Error getting die value!");
        return 0;
    }
}