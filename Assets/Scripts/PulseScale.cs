using DG.Tweening;
using TMPro;
using UnityEngine;

public class PulseScale : MonoBehaviour
{
    [SerializeField]        
    float _pulseDuration = 1f;
    [SerializeField]
    Vector3 _pulseScale = new Vector3(1.2f, 1.2f, 1.2f);

    Tween _pulseTween;

    public void StopPulse()
    {
        if (_pulseTween == null || !_pulseTween.IsActive()) return;
        _pulseTween.Kill();
        _pulseTween = null;
        transform.localScale = Vector3.one;
    }
    
    public void StartPulse()
    {
        _pulseTween = transform.DOScale(_pulseScale, _pulseDuration)
            .SetEase(Ease.OutQuad)
            .OnComplete(ReversePulseScale);
    }

    private void ReversePulseScale()
    {
        _pulseTween = transform.DOScale(Vector3.one, _pulseDuration)
            .SetEase(Ease.OutQuad)
            .OnComplete(StartPulse);
    }
}
