﻿using System.Collections.Generic;
using UnityEngine;

public static class Helpers
{
    static Dictionary<float, WaitForSeconds> _waitForSeconds = new Dictionary<float, WaitForSeconds>();

    /// <summary>
    /// Cache WaitForSeconds for reuse to prevent creating garbage.
    /// </summary>
    /// <param name="seconds">Value in seconds for WaitForSeconds.</param>
    /// <returns>
    /// A <see cref="WaitForSeconds"/> instead with the specified value.
    /// This will either come from the dictionary or be instantiated if we don't
    /// already have one
    /// </returns>
    /// <example>
    /// Usage:
    /// IEnumerator SomeCoroutine()
    /// {
    ///     yield return Helpers.GetWaitForSeconds(3f);
    ///     /// do something ...
    /// }
    /// </example>
    public static WaitForSeconds GetWaitForSeconds(float seconds)
    {
        if (_waitForSeconds.ContainsKey(seconds)) return _waitForSeconds[seconds];
        var waitForSeconds = new WaitForSeconds(seconds);
        _waitForSeconds.Add(seconds, waitForSeconds);
        return _waitForSeconds[seconds];
    }
}