using UnityEngine;

public class ChessBoardSquare : MonoBehaviour
{
    [SerializeField] float _gizmoSize = 0.9f;
    [SerializeField] Color _gizmoColor = new Color(1, 0.92f, 0.016f, 0.25f);

    void OnDrawGizmosSelected()
    {
        Gizmos.color = _gizmoColor;
        Gizmos.DrawCube(transform.position, _gizmoSize * Vector3.one);
    }
}
