﻿public enum Rank
{
    Pawn,
    Rook,
    Knight,
    Bishop,
    Queen,
    King
}