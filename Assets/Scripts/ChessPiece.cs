using UnityEngine;

public class ChessPiece : MonoBehaviour
{
    public PieceColor PieceColor => _pieceColor;
    public Rank Rank => _rank;
    
    [SerializeField] PieceColor _pieceColor;
    [SerializeField] Rank _rank;
}