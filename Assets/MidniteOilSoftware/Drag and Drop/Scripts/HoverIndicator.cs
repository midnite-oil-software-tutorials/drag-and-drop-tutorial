using UnityEngine;

namespace MidniteOilSoftware
{
    [RequireComponent(typeof(MouseInputBase))]
    public class HoverIndicator : MonoBehaviour
    {
        [SerializeField] Vector3 _hoverScale = new Vector3(1.1f, 1.1f, 1.1f);

        Clickable _clickable;

        private void Start()
        {
            _clickable = GetComponent<Clickable>();
            if (!_clickable) return;
            _clickable.OnMouseEntered += OnMouseEntered;
            _clickable.OnMouseExited += OnMouseExited;
        }

        private void OnDisable()
        {
            if (!_clickable) return;
            _clickable.OnMouseEntered -= OnMouseEntered;
            _clickable.OnMouseExited -= OnMouseExited;
        }

        void OnMouseEntered(GameObject _)
        {
            transform.localScale = _hoverScale;
        }

        void OnMouseExited(GameObject _)
        {
            transform.localScale = Vector3.one;
        }
    }
}
